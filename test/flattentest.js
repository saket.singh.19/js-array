const flat=require('../flatten.js')

let nestedArray = [1, [2], [[3]], [[[4]]]];
let nestedArray2 = [3, [4], [[6]], [[[8]]]];

let result = flat(nestedArray);
let result2 = flat(nestedArray2);

console.log(result);
console.log(result2);